class LoansController < ApplicationController
  before_action :require_login

  def index
  end

  def new
    @loan = Loan.new
  end

  def create
    @loan = Loan.new(loan_params)
    # abort loan_params
    @loan.user = current_user
    if @loan.save
      redirect_to @loan
    else
      render 'new'
    end
  end

  def show
    @loan = Loan.find(params[:id])
  end

  private

    def loan_params
      params.require(:loan).permit(:card, :person)
    end
end
