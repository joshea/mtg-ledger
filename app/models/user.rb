class User < ApplicationRecord
  include Clearance::User

  has_many :active_friendships, class_name:  "Friendship",
                                  foreign_key: "friender_id",
                                  dependent:   :destroy

end
