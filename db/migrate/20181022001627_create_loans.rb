class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.references :user
      t.string :card
      t.string :person

      t.timestamps
    end
  end
end
