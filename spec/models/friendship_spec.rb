require 'rails_helper'

RSpec.describe Friendship, type: :model do

  before :each do
    @sam = create_user('sam@example.com')
    @bob = create_user('bob@example.com')
    @friendship = FactoryBot.create(:friendship, friender_id: @sam.id, friended_id: @bob.id)
  end

  it "should be valid" do
    expect(@friendship).to be_valid
  end

  it "should require a friender_id" do
    @friendship.friender_id = nil
    expect(@friendship).not_to be_valid
  end

  it "should require a friended_id" do
    @friendship.friended_id = nil
    expect(@friendship).not_to be_valid
  end

  private
    def create_user(email)
      FactoryBot.create(:user, email: email)
    end
end
