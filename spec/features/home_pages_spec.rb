require 'rails_helper'

RSpec.feature "HomePages", type: :feature do
  scenario 'users can visit the home page' do
    visit root_path

    expect(page).to have_text 'MTG Ledger'
  end
end
