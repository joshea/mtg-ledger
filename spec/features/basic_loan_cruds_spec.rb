require 'rails_helper'
require 'clearance/rspec'

RSpec.feature 'BasicLoanCruds', type: :feature do

  # before(:each) do
  #   sign_in
  # end

  # scenario 'users can see their loans' do
  #   visit loans_path
  #   expect(page).to have_text 'Your Loans'
  # end

  scenario 'can create new loan' do
    # visit new_loan_path(as: user)
    sign_in
    visit new_loan_path
    expect(page).to have_text 'Add Loan'

    card = 'Dimir Spybug'
    person = 'Joey'

    fill_in 'loan_card', with: card
    fill_in 'loan_person', with: person

    click_button 'Save this loan'
  end
end
