Rails.application.routes.draw do
  get 'loans/index'
  get 'welcome/index'

  root 'welcome#index'

  resources :loans
  resources :friends
end
