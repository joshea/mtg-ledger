## MTG Ledger

### Development Environment
This project provides a development environment via `docker-compose`.

It also provides several scripts in `s/` to assist in the development process.

To start using this project:
- clone this repository; `cd` to the root of the project directory
- to build the docker image, run `s/build`
- to spin up the local development server, run `s/run`
- your development environment is now available at `0.0.0.0:3000`

The development environment uses `webpack-dev-server` for compiling JS and CSS. It uses live reloading, so whenever you change a file in `app/javascript/application` the page should automatically reload to reflect the changes.

You can restart stop the development server by running `s/stop`

#### Aliases
This project provides a suggested set of helpful aliases in `.aliases`. To use them, run `source .aliases` in your shell.

See `.aliases` for the list of aliases you can use. Examples:
- `spec` runs the rails rspec test suite
- `re` rebuilds your development environment (short for `s/rebuild`)

#### Other utilities
If you need to run a command inside the development container, use `s/dr YOUR_COMMAND`; examples:
- if you need to run `rails spec`: `s/dr rails spec`
- if you need to run `rails db:migrate`: `s/dr rails db:migrate`

If you need to quickly restart the development environment, you can run `s/bounce`

If you need to run the development servers while seeing the output, run `s/debug`

## CI/CD with Auto DevOps

This project is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). It has been extended and customize to fit the needs of this project. See `.gitlab-ci.yml` and `Dockerfile` for details on the build process.

Right now, production is deployed here: http://joshea-mtg-ledger.apps.devkops.fluxfocused.com/
